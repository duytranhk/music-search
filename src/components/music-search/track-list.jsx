import React, { Component } from 'react';
import styled from 'styled-components';

const TrackListContainer = styled.div`
    display: flex;
    width:100%;
    justify-content: space-around;
    flex-wrap:wrap;
    margin-top: 1rem;
`
const TrackItem = styled.div`
    width:250px;
    height:250px;
    margin-bottom: 1.5rem;
    background-color: #FFF;
    box-shadow: 0 10px 30px 0 rgba(213,227,239,.6);
    border-radius: 8px;
    padding: .5rem;
    display: flex;
    flex-direction: column;
`
const TrackImage = styled.img`
    -webkit-filter: blur(.8px);
    filter: blur(.8px);
    osition: absolute;
    width: 230px;
    height: 230px;
`

const TrackName = styled.div`
    position: absolute;
    text-align: center;
    background-color: #0000006b;
    padding: .5rem;
    width: 230px;
    color: #FFF;
`
const TrackPlayButton = styled.button`
    justify-self: center;
    align-self: center;
    position: absolute;
    margin-top: 115px;
`

let player = new Audio();
export default class TrackList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tracks: [],
            isPlaying: false,
            currentPlayingUrl: "",
            player: null
        }
    }

    componentWillMount = () => {
        this.setState({
            tracks: this.props.tracks
        })
    }

    componentWillReceiveProps = (nextProps) => {
        this.setState({
            tracks: nextProps.tracks
        })
    }

    playAudio = (url) => {
        let player = new Audio(url);
        if (!this.state.isPlaying) {
            player.play();
            this.setState({
                isPlaying: true,
                currentPlayingUrl: url,
                player
            })
        } else {
            if (this.state.currentPlayingUrl == url) {
                this.state.player.pause();
                this.setState({
                    isPlaying: false
                }); ``
            } else {
                this.state.player.pause();
                player.play();
                this.setState({
                    isPlaying: true,
                    currentPlayingUrl: url,
                    player
                })
            }
        }
    }
    render = () => {
        return (
            <TrackListContainer>
                {
                    this.state.tracks.map((track, key) => {
                        return (
                            <TrackItem key={key}>
                                <TrackImage src={track.album.images[0].url} />
                                <TrackName>{track.name}</TrackName>
                                <TrackPlayButton onClick={() => this.playAudio(track.preview_url)}>
                                    {
                                        this.state.currentPlayingUrl == track.preview_url && this.state.isPlaying ?
                                            'Pause' : 'Play'
                                    }
                                </TrackPlayButton>
                            </TrackItem>
                        );
                    })
                }
            </TrackListContainer>
        )
    }
}