import React, { Component } from 'react';
import styled from 'styled-components';
import { InputGroup, InputGroupAddon, Input } from 'reactstrap';
import ArtistProfile from './artist-profile'
import SpotifyWebApi from 'spotify-web-api-js'
const SearchContainer = styled.div`
    display: inline-flex;
    flex-wrap: wrap;
    flex-direction: column;
    width:100%;
`
const SearchInputRow = styled.div`
    width: 50%;
    margin: auto;
`
const InputGroupAddonCustom = styled(InputGroupAddon) `
    cursor: pointer;
`

var spotifyService = new SpotifyWebApi();

export default class MusicSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchInput: "",
            artist: null,
            tracks: null
        }

        this.search = this.search.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentWillMount = () => {
        let accessToken = sessionStorage.getItem('access_token');
        spotifyService.setAccessToken(accessToken);
    }

    search = async () => {
        let repsonse = await spotifyService.searchArtists(this.state.searchInput, { limit: 1 });
        if (repsonse.artists.items.length > 0) {
            let artist = repsonse.artists.items[0];
            let tracks = await spotifyService.getArtistTopTracks(artist.id, 'us');
            this.setState({
                artist,
                tracks: tracks.tracks
            });
        }
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.search();
        }
    }

    render() {
        return (
            <div className="container mt-3">
                <SearchContainer>
                    <SearchInputRow>
                        <InputGroup>
                            <Input placeholder="Search for music"
                                onChange={event => this.setState({ searchInput: event.target.value })}
                                onKeyPress={this.handleKeyPress} />
                            <InputGroupAddonCustom addonType="append" onClick={() => this.search()}>GO</InputGroupAddonCustom>
                        </InputGroup>
                    </SearchInputRow>
                </SearchContainer>
                <ArtistProfile artist={this.state.artist} tracks={this.state.tracks} />
            </div>
        );
    }
}