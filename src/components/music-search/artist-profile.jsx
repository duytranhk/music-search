import React, { Component } from 'react';
import styled from 'styled-components';
import TrackList from './track-list';

const ArtistProfileContainer = styled.div`
    width: 100%;
    padding: 1rem 0;
    display: inline-flex;
    flex-wrap: wrap;
`
const AvatarBox = styled.div`
    width:100%;
    text-align:center;
`
const Avatar = styled.img`
    width: 20%;
    clip-path: circle(at center);
`

const InfoBox = styled.div`
    width:100%;
    justify-content: center;
    display: flex;
    flex-direction: column;
    text-align:center;
    margin-bottom: 2rem;
`

const Follower = styled.span`
    font-size: 13px;
    color: #999;
    font-weight: 500;
`

const Name = styled.h2`
    font-weight: 500;
    color: #304e6d;
`
export default class ArtistProfile extends Component {
    constructor(props) {
        super(props);
    }
    

    render = () => {
        return (
            this.props.artist ?
                (<ArtistProfileContainer>
                    <AvatarBox>
                        {
                            this.props.artist.images.length > 0 ?
                                (<Avatar src={this.props.artist.images[0].url} />) :
                                (<Avatar src="http://hotchillitri.co.uk/wp-content/uploads/2016/10/empty-avatar.jpg" />)
                        }
                    </AvatarBox>
                    <InfoBox>
                        <Name>{this.props.artist.name}</Name>
                        <Follower>{this.props.artist.followers.total} followers</Follower>
                        <span>
                            {
                                this.props.artist.genres.map((gen, key) => {
                                    if (key !== this.props.artist.genres.length - 1)
                                        return `${gen}, `
                                    else
                                        return `${gen}`
                                })
                            }
                        </span>
                    </InfoBox>
                    <TrackList tracks={this.props.tracks} />
                </ArtistProfileContainer>) :
                (this.props.artist === undefined ? <div className="text-center"><small>Not Found</small></div> : <div></div>)
        );
    }
}