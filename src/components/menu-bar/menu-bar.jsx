import React, { Component } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';
import appConfig from '../../appConfig';


export default class MenuBar extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);

        this.state = {
            isOpen: false,
            loginUrl: ""
        };
    }
    componentWillMount = () => {
        const API_URL = "https://accounts.spotify.com/en/authorize";
        const loginUrl = `${API_URL}?client_id=${appConfig.cliendId}&response_type=code&redirect_uri=http://${window.location.hostname}:${window.location.port}`;
        this.setState({ loginUrl });
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    logOut = () => {
        sessionStorage.clear();
        window.location.href = '/';
    }
    render() {
        return (
            <header>
                <Navbar color="dark" dark expand="md">
                    <div className="container">
                        <NavbarBrand href="/">Music Search</NavbarBrand>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    {
                                        this.props.isLogin ?
                                        (<NavLink onClick={() => this.logOut()} style={{cursor:'pointer'}}>Sign Out</NavLink>) :
                                        (<NavLink href={this.state.loginUrl}>Sign In</NavLink>)
                                    }
                                    
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
            </header>
        );
    }
}