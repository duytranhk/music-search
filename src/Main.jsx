import React, { Component } from 'react';
import { MenuBar, MusicSearch } from './components';
import UtilitiesService from './services/utilities.service'
import styled from 'styled-components';
import appConfig from './appConfig';
import $ from 'jquery';
const NotLogin = styled.div`
    text-align: center;
`



export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: false
        }
    }

    componentWillMount = () => {
        let accessToken = sessionStorage.getItem('access_token')
        if (!accessToken) {
            let authCode = UtilitiesService.prototype.getQueryString('code')
            this.getAccessToken(authCode, () => {
                this.setState({ isLogin: true })
            })
        } else {
            this.setState({ isLogin: true })
        }
    }

    getAccessToken = (authCode, callback) => {
        if (authCode) {
            let data = {
                grant_type: 'authorization_code',
                code: authCode,
                redirect_uri: `http://${window.location.hostname}:${window.location.port}`,
                client_id: appConfig.cliendId,
                client_secret: appConfig.secretKey
            }
            fetch('api/token', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: $.param(data)
            }).then(response => response.json()).then(json => {
                sessionStorage.setItem('access_token', json.access_token);
                sessionStorage.setItem('refresh_token', json.refresh_token);
                sessionStorage.setItem('token_type', json.token_type);

                callback();
            });
        }

    }

    render() {
        return (
            <div>
                <MenuBar isLogin={this.state.isLogin}/>
                <main role="main">
                    {
                        this.state.isLogin ? 
                        (<MusicSearch />) :
                        (<NotLogin className="container mt-3">Please login</NotLogin>)
                    }
                </main>
            </div>
        );
    };
}